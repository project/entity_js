Entity JS
--------------------------
This module provides JavaScript access
to some common Entity API functions.

//Return a fully rendered entity using a view mode
entity_render_view(entity_type, entity_id, view_mode);

//Return entity object as JSON string
entity_load_json(entity_type, entity_id);

Note: This module does not yet have advanced permissions.

/////////-/////////
USE WITH CAUTION!!!
\\\\\\\\\-\\\\\\\\\
